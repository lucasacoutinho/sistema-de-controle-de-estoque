<?php

namespace Domain\Categorias;

class Categorias
{
    public const NOTEBOOK            = 'Notebook';
    public const DESKTOP             = 'Desktop';
    public const SERVIDOR            = 'Servidor';
    public const SWITCH              = 'Switch';
    public const ACCESSPOINT         = 'Access point';
    public const RACK                = 'Rack';
    public const NOBREAK             = 'Nobreak';
    public const MONITOR             = 'Monitor';
    public const TABLET              = 'Tablet';
    public const SMARTPHONE          = 'Smartphone';
    public const VOIP                = 'VoIP';
    public const CAMERA              = 'Câmera';
    public const SO                  = 'Sistema Operacional';
    public const OFFICE              = 'Ferramenta de escritório (Office)';
    public const ANTIVIRUS           = 'Antivírus';
    public const CAD                 = 'Ferramenta CAD';
    public const VIDEOEDITING        = 'Edição de Vídeo';
    public const IMAGEEDITING        = 'Edição de imagens';
    public const DATABASE            = 'Banco de dados';
    public const SOFTWAREDEVELOPMENT = 'Ferramenta de Desenvolvimento de Software';
    public const SIG                 = 'Sistema de Informação Geográfica (SIG)';
    public const PROCESSOR           = 'Processador';
    public const MOTHERBOARD         = 'Placa mãe';
    public const MEMORY              = 'Memória RAM';
    public const PSU                 = 'Fonte de Energia';
    public const HD                  = 'HD';
    public const EXTERNALHD          = 'HD Externo';
    public const SSD                 = 'SSD';
    public const EXTERNALSSD         = 'SSD Externo';
    public const DRIVERDISK          = 'Drive de CD/DVD/Blu-ray';
    public const KEYBOARD            = 'Teclado';
    public const MOUSE               = 'Mouse';
    public const NETWORKCABLE        = 'Cabo de rede';
    public const AUDIOBOX            = 'Caixas de som';
    public const GRAPHICSCARD        = 'Placa de Vídeo';
    public const NETWORKCARD         = 'Placa de Rede';
    public const PENDRIVE            = 'Pen Drive';
    public const OTHERSOFTWARE       = 'Outros Softwares';
    public const OTHERHARDWARE       = 'Outros Hardwares';
}
