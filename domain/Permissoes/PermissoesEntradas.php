<?php

namespace Domain\Permissoes;

class PermissoesEntradas
{
    public const INDEX   = 'entrada-index';
    public const STORE   = 'entrada-store';
    public const UPDATE  = 'entrada-update';
    public const DESTROY = 'entrada-destroy';
}
