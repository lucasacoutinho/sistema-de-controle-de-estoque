<?php

if (!function_exists('authenticatedUserHasPermission')) {
    function authenticatedUserHasPermission(string $permission): bool
    {
        return getAuthenticatedUser()->can($permission);
    }
}

if (!function_exists('authenticatedUserPermissions')) {
    function authenticatedUserPermissions(): array
    {
        $usuario = getAuthenticatedUser();

        if(!$usuario) {
            return [];
        }

        if($usuario->roles()->get()->isEmpty()) {
            return [];
        }

        if($usuario->roles()->first()->permissions()->get()->isEmpty()) {
            return [];
        }

        return $usuario->roles()->first()->permissions()->get()->pluck('name')->toArray();
    }
}
