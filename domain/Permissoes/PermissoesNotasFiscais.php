<?php

namespace Domain\Permissoes;

class PermissoesNotasFiscais
{
    public const INDEX   = 'notas-fiscais-index';
    public const STORE   = 'notas-fiscais-store';
    public const UPDATE  = 'notas-fiscais-update';
    public const DESTROY = 'notas-fiscais-destroy';
}
