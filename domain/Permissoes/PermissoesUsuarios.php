<?php

namespace Domain\Permissoes;

class PermissoesUsuarios
{
    public const INDEX   = 'usuario-index';
    public const STORE   = 'usuario-store';
    public const UPDATE  = 'usuario-update';
    public const DESTROY = 'usuario-destroy';
}
