<?php

namespace Domain\Permissoes;

class PermissoesPermissoes
{
    public const INDEX   = 'permissao-index';
    public const STORE   = 'permissao-store';
    public const UPDATE  = 'permissao-update';
    public const DESTROY = 'permissao-destroy';
}
