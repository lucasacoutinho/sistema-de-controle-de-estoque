<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompraNotaFiscalTable extends Migration
{
    public function up()
    {
        Schema::create('compra_nota_fiscal', function (Blueprint $table) {
            $table->id();
            $table->foreignId('fornecedor_id')->constrained('fornecedores')->onUpdate('cascade')->onDelete('cascade');
            $table->string('numero_nota_fiscal');
            $table->timestamp('comprado_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('compra_nota_fiscal');
    }
}
