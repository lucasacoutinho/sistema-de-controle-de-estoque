<?php

namespace Database\Seeders;

use App\Models\Categoria;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Domain\Categorias\Categorias;

class CategoriaSeeder extends Seeder
{
    public function run()
    {
        $this->criarCategoria(Categorias::NOTEBOOK);
        $this->criarCategoria(Categorias::DESKTOP);
        $this->criarCategoria(Categorias::SERVIDOR);
        $this->criarCategoria(Categorias::SWITCH);
        $this->criarCategoria(Categorias::ACCESSPOINT);
        $this->criarCategoria(Categorias::RACK);
        $this->criarCategoria(Categorias::NOBREAK);
        $this->criarCategoria(Categorias::MONITOR);
        $this->criarCategoria(Categorias::TABLET);
        $this->criarCategoria(Categorias::SMARTPHONE);
        $this->criarCategoria(Categorias::VOIP);
        $this->criarCategoria(Categorias::CAMERA);
        $this->criarCategoria(Categorias::SO);
        $this->criarCategoria(Categorias::OFFICE);
        $this->criarCategoria(Categorias::ANTIVIRUS);
        $this->criarCategoria(Categorias::CAD);
        $this->criarCategoria(Categorias::VIDEOEDITING);
        $this->criarCategoria(Categorias::IMAGEEDITING);
        $this->criarCategoria(Categorias::DATABASE);
        $this->criarCategoria(Categorias::SOFTWAREDEVELOPMENT);
        $this->criarCategoria(Categorias::SIG);
        $this->criarCategoria(Categorias::PROCESSOR);
        $this->criarCategoria(Categorias::MOTHERBOARD);
        $this->criarCategoria(Categorias::MEMORY);
        $this->criarCategoria(Categorias::PSU);
        $this->criarCategoria(Categorias::HD);
        $this->criarCategoria(Categorias::EXTERNALHD);
        $this->criarCategoria(Categorias::SSD);
        $this->criarCategoria(Categorias::EXTERNALSSD);
        $this->criarCategoria(Categorias::DRIVERDISK);
        $this->criarCategoria(Categorias::KEYBOARD);
        $this->criarCategoria(Categorias::MOUSE);
        $this->criarCategoria(Categorias::NETWORKCABLE);
        $this->criarCategoria(Categorias::AUDIOBOX);
        $this->criarCategoria(Categorias::GRAPHICSCARD);
        $this->criarCategoria(Categorias::NETWORKCARD);
        $this->criarCategoria(Categorias::PENDRIVE);
        $this->criarCategoria(Categorias::OTHERSOFTWARE);
        $this->criarCategoria(Categorias::OTHERHARDWARE);
    }

    public function criarCategoria(string $categoria)
    {
        Categoria::create([
            'titulo' => Str::lower($categoria)
        ]);
    }
}
