<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\ScopeSessions;
use App\Http\Controllers\Home\RedirectController;
use App\Http\Controllers\Central\Home\HomeController;
use App\Http\Controllers\Central\Auth\LoginController;
use App\Http\Controllers\Central\Ativo\AtivoController;
use App\Http\Controllers\Central\Compra\CompraController;
use App\Http\Controllers\Central\Funcao\FuncaoController;
use App\Http\Controllers\Central\Unidade\UnidadeController;
use App\Http\Controllers\Central\Usuario\UsuarioController;
use App\Http\Controllers\Central\Compra\NotaFiscalController;
use App\Http\Controllers\Central\Auth\ResetPasswordController;
use App\Http\Controllers\Central\Auth\ForgotPasswordController;
use App\Http\Controllers\Central\Categoria\CategoriaController;
use App\Http\Controllers\Central\Permissao\PermissaoController;
use App\Http\Controllers\Central\Fornecedor\FornecedorController;
use App\Http\Controllers\Central\Transferencia\TransferenciaController;

Route::get('/', [RedirectController::class, 'index']);

Route::middleware([ScopeSessions::class])->prefix('admin')->group(function () {
    Route::name('central.')->group(function () {
        Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
        Route::post('login', [LoginController::class, 'login']);

        #Registration
        #Route::get('register', [RegisterController::class, 'showRegistrationForm'])->name('register');
        #Route::post('register', [RegisterController::class, 'register']);

        Route::prefix('password')->name('password.')->group(function () {
            Route::get('reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('request');
            Route::post('email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('email');

            Route::get('reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('reset');
            Route::post('reset', [ResetPasswordController::class, 'reset'])->name('update');
        });

        Route::middleware(['auth'])->group(function () {
            Route::post('logout', [LoginController::class, 'logout'])->name('logout');

            Route::get('/', [HomeController::class, 'index'])->name('home');
            Route::apiResource('unidades', UnidadeController::class)->parameters(['unidades' => 'unidade']);
            Route::apiResource('usuarios', UsuarioController::class)->parameters(['usuarios' => 'usuario']);
            Route::apiResource('categorias', CategoriaController::class)->parameters(['categorias' => 'categoria']);
            Route::apiResource('ativos', AtivoController::class)->parameters(['ativos' => 'ativo']);
            Route::apiResource('fornecedores', FornecedorController::class)->parameter('fornecedores', 'fornecedor');
            Route::apiResource('notas-fiscais', NotaFiscalController::class);
            Route::apiResource('compras', CompraController::class)->parameters(['compras' => 'compra']);
            Route::apiResource('transferencias', TransferenciaController::class)->parameter('transferencias', 'transferencia');
            Route::apiResource('permissoes', PermissaoController::class)->parameter('permissoes', 'permissao');
            Route::apiResource('funcoes', FuncaoController::class)->parameter('funcoes', 'funcao');
        });
    });
});
