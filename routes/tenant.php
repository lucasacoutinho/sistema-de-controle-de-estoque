<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\ScopeSessions;
use App\Http\Controllers\Unidade\Home\HomeController;
use App\Http\Controllers\Unidade\Auth\LoginController;
use Stancl\Tenancy\Middleware\InitializeTenancyByPath;
use App\Http\Controllers\Unidade\Ativo\AtivoController;
use App\Http\Controllers\Unidade\Auth\RegisterController;
use App\Http\Controllers\Unidade\Entrada\EntradaController;
use App\Http\Controllers\Unidade\Usuario\UsuarioController;
use App\Http\Controllers\Unidade\Auth\ResetPasswordController;
use App\Http\Controllers\Unidade\Auth\ForgotPasswordController;
use App\Http\Controllers\Unidade\Transferencia\TransferenciaController;

/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here you can register the tenant routes for your application.
| These routes are loaded by the TenantRouteServiceProvider.
|
| Feel free to customize them however you want. Good luck!
|
*/

Route::middleware([
    'web',
    InitializeTenancyByPath::class,
    ScopeSessions::class,
])->prefix('/unidade/{tenant}/')->group(function () {
    Route::name('unidade.')->group(function() {
        Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
        Route::post('login', [LoginController::class, 'login']);

        // Registration
        Route::get('register', [RegisterController::class, 'showRegistrationForm'])->name('register');
        Route::post('register', [RegisterController::class, 'register']);

        // Password Reset
        Route::prefix('password')->name('password.')->group(function() {
            Route::get('reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('request');
            Route::post('email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('email');

            Route::get('reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('reset');
            Route::post('reset', [ResetPasswordController::class, 'reset'])->name('update');
        });

        Route::middleware(['auth'])->group(function () {
            Route::post('logout', [LoginController::class, 'logout'])->name('logout');

            Route::get('/', [HomeController::class, 'index'])->name('home');
            Route::apiResource('ativos', AtivoController::class)->parameters(['ativos' => 'ativo']);
            Route::apiResource('entradas', EntradaController::class)->parameters(['entradas' => 'entrada']);
            Route::apiResource('transferencias', TransferenciaController::class)->parameters(['transferencias' => 'transferencia']);
            Route::apiResource('usuarios', UsuarioController::class)->parameters(['usuarios' => 'usuario']);
        });
    });
});
