<?php

namespace App\Http\Middleware;

use Inertia\Middleware;
use Illuminate\Http\Request;

class HandleInertiaRequests extends Middleware
{
    protected $rootView = 'app';

    public function version(Request $request)
    {
        return parent::version($request);
    }

    public function share(Request $request)
    {
        return array_merge(parent::share($request), [
            'appName'    => config('app.name'),
            'auth'       => [
                'permissoes' => getAuthenticatedUser() ? authenticatedUserPermissions() : null,
            ],
            'unidade'   => tenant('id'),
            'flash'     => [
                'message' => [
                    'success' => fn () => $request->session()->get('success'),
                    'error'   => fn () => $request->session()->get('error'),
                    'info'    => fn () => $request->session()->get('info'),
                    'warning' => fn () => $request->session()->get('warning'),
                ]
            ]
        ]);
    }
}
