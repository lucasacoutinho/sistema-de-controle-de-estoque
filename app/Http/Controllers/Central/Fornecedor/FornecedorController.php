<?php

namespace App\Http\Controllers\Central\Fornecedor;

use Inertia\Inertia;
use App\Models\Fornecedor;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Domain\Permissoes\PermissoesFornecedores;
use App\Http\Requests\Central\Fornecedor\FornecedorStoreRequest;
use App\Http\Requests\Central\Fornecedor\FornecedorUpdateRequest;

class FornecedorController extends Controller
{
    public function index()
    {
        abort_unless(authenticatedUserHasPermission(PermissoesFornecedores::INDEX), 403);

        $fornecedores = Fornecedor::orderByDesc('created_at')->get()->map(function ($fonecedor) {
            return [
                'id'         => $fonecedor->id,
                'titulo'     => $fonecedor->titulo,
                'created_at' => $fonecedor->created_at->format('d-m-Y H:i:s'),
                'updated_at' => $fonecedor->updated_at->format('d-m-Y H:i:s'),
            ];
        });

        return Inertia::render('Central/Fornecedor/Index', [
            'fornecedores' => $fornecedores,
            'permissoes' => [
                'store'   => PermissoesFornecedores::STORE,
                'update'  => PermissoesFornecedores::UPDATE,
                'destroy' => PermissoesFornecedores::DESTROY,
            ]
        ]);
    }

    public function store(FornecedorStoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            Fornecedor::create($request->validated());
        });

        return Redirect::route('central.fornecedores.index')->with('success', 'Fornecedor criado com sucesso!');
    }

    public function update(FornecedorUpdateRequest $request, Fornecedor $fornecedor)
    {
        DB::transaction(function () use ($request, $fornecedor) {
            $fornecedor->update($request->validated());
        });

        return Redirect::route('central.fornecedores.index')->with('success', 'Fornecedor criado com sucesso!');
    }

    public function destroy(Fornecedor $fornecedor)
    {
        abort_unless(authenticatedUserHasPermission(PermissoesFornecedores::DESTROY), 403);

        if($fornecedor->notasFiscais()->get()->isNotEmpty()) {
            return Redirect::route('central.fornecedores.index')->with('error', 'Não é possível excluir um fornecedor que emitiu notas fiscais!');
        }

        $fornecedor->delete();

        return Redirect::route('central.fornecedores.index')->with('success', 'Fornecedor excluido com sucesso!');
    }
}
