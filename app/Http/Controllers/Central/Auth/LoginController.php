<?php

namespace App\Http\Controllers\Central\Auth;

use Inertia\Inertia;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Symfony\Component\HttpFoundation\JsonResponse;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = RouteServiceProvider::CENTRAL_DASHBOARD;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function attemptLogin(Request $request)
    {
        $credentials = $this->credentials($request);

        $credentials = Arr::add($credentials, 'tenant_id', null);

        return $this->guard()->attempt($credentials, $request->filled('remember'));
    }

    public function showLoginForm()
    {
        return Inertia::render('Central/Auth/Login');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return $request->wantsJson() ? new JsonResponse([], 204) : redirect()->route('central.home');
    }
}
