<?php

namespace App\Http\Controllers\Central\Auth;

use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    protected $redirectTo = RouteServiceProvider::CENTRAL_DASHBOARD;

    public function showResetForm(Request $request)
    {
        $token = $request->route()->parameter('token');

        return Inertia::render('Central/Auth/Passwords/Reset', [
            'token' => $token,
            'email' => $request->email
        ]);
    }
}
