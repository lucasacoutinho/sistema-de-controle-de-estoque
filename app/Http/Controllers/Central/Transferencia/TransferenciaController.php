<?php

namespace App\Http\Controllers\Central\Transferencia;

use Inertia\Inertia;
use App\Models\Ativo;
use Domain\Status\Status;
use App\Models\Transferencia;
use App\Models\Tenant as Unidade;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Domain\Permissoes\PermissoesTransferencias;
use App\Http\Requests\Central\Transferencia\TransferenciaStoreRequest;
use App\Http\Requests\Central\Transferencia\TransferenciaUpdateRequest;

class TransferenciaController extends Controller
{
    public function index()
    {
        abort_unless(authenticatedUserHasPermission(PermissoesTransferencias::INDEX), 403);

        $ativos = Ativo::all()->map(function($ativo) {
            return ['id' => $ativo->id, 'titulo' => $ativo->titulo];
        });

        $unidades = Unidade::all()->map(function($unidade) {
            return ['id' => $unidade->id];
        });

        $transferencias = Transferencia::orderByDesc('created_at')->get()->map(function ($transferencia) {
            return [
                'id'             => $transferencia->id,
                'ativo_id'       => $transferencia->ativo_id,
                'unidade'        => $transferencia->unidade_destino,
                'quantidade'     => $transferencia->quantidade,
                'status'         => $transferencia->status,
                'created_at'     => $transferencia->created_at->format('d-m-Y H:i:s'),
                'updated_at'     => $transferencia->updated_at->format('d-m-Y H:i:s'),
            ];
        });

        return Inertia::render('Central/Transferencia/Index', [
            'transferencias' => $transferencias,
            'unidades'       => $unidades,
            'ativos'         => $ativos,
            'permissoes'     => [
                'store'   => PermissoesTransferencias::STORE,
                'update'  => PermissoesTransferencias::UPDATE,
                'destroy' => PermissoesTransferencias::DESTROY,
            ]
        ]);
    }

    public function store(TransferenciaStoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            Transferencia::create($request->validated());
        });

        return Redirect::route('central.transferencias.index')->with('success', 'Transferência criada com sucesso!');
    }

    public function update(TransferenciaUpdateRequest $request, Transferencia $transferencia)
    {
        if($transferencia->unidade_origem !== null) {
            return Redirect::route('central.transferencias.index')->with('error', 'Não é possível alterar uma transferência realizada em outra unidade!');
        }

        if ($transferencia->status !== Status::PENDENTE) {
            return Redirect::route('central.transferencias.index')->with('error', 'Não é possível alterar uma transferência que foi confirmada/negada!');
        }

        DB::transaction(function () use ($request, $transferencia) {
            $transferencia->update($request->validated());
        });

        return Redirect::route('central.transferencias.index')->with('success', 'Transferência atualizada com sucesso!');
    }

    public function destroy(Transferencia $transferencia)
    {
        abort_unless(authenticatedUserHasPermission(PermissoesTransferencias::DESTROY), 403);

        if($transferencia->unidade_origem !== null) {
            return Redirect::route('central.transferencias.index')->with('error', 'Não é possível excluir uma transferência realizada em outra unidade!');
        }

        if ($transferencia->status !== Status::PENDENTE) {
            return Redirect::route('central.transferencias.index')->with('error', 'Não é possível excluir uma transferência que foi confirmada/negada!');
        }

        $transferencia->delete();

        return Redirect::route('central.transferencias.index')->with('success', 'Transferência excluida com sucesso!');
    }
}
