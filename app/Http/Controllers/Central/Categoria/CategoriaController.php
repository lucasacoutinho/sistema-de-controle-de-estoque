<?php

namespace App\Http\Controllers\Central\Categoria;

use Inertia\Inertia;
use App\Models\Categoria;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Domain\Permissoes\PermissoesCategorias;
use App\Http\Requests\Central\Categoria\CategoriaStoreRequest;
use App\Http\Requests\Central\Categoria\CategoriaUpdateRequest;

class CategoriaController extends Controller
{
    public function index()
    {
        abort_unless(authenticatedUserHasPermission(PermissoesCategorias::INDEX), 403);

        $categorias = Categoria::orderByDesc('created_at')->get()->map(function ($categoria) {
            return [
                'id'         => $categoria->id,
                'titulo'     => $categoria->titulo,
                'created_at' => $categoria->created_at->format('d-m-Y H:i:s'),
                'updated_at' => $categoria->updated_at->format('d-m-Y H:i:s'),
            ];
        });

        return Inertia::render('Central/Categoria/Index', [
            'categorias' => $categorias,
            'permissoes' => [
                'store'   => PermissoesCategorias::STORE,
                'update'  => PermissoesCategorias::UPDATE,
                'destroy' => PermissoesCategorias::DESTROY,
            ]
        ]);
    }

    public function store(CategoriaStoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            Categoria::create($request->validated());
        });

        return Redirect::route('central.categorias.index')->with('success', 'Categoria criada com sucesso!');
    }

    public function update(CategoriaUpdateRequest $request, Categoria $categoria)
    {
        DB::transaction(function () use ($request, $categoria) {
            $categoria->update($request->validated());
        });

        return Redirect::route('central.categorias.index')->with('success', 'Categoria criada com sucesso!');
    }

    public function destroy(Categoria $categoria)
    {
        abort_unless(authenticatedUserHasPermission(PermissoesCategorias::DESTROY), 403);

        $categoria->delete();

        return Redirect::route('central.categorias.index')->with('success', 'Categoria excluida com sucesso!');
    }
}
