<?php

namespace App\Http\Controllers\Central\Compra;

use Inertia\Inertia;
use App\Models\Ativo;
use App\Models\NotaFiscal;
use App\Models\AtivoCompra;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Domain\Permissoes\PermissoesCompras;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\Central\Compra\AtivoCompraStoreRequest;
use App\Http\Requests\Central\Compra\AtivoCompraUpdateRequest;

class CompraController extends Controller
{
    public function index()
    {
        abort_unless(authenticatedUserHasPermission(PermissoesCompras::INDEX), 403);

        $ativos = Ativo::all()->map(function($ativo) {
            return [
                'id'     => $ativo->id,
                'titulo' => $ativo->titulo
            ];
        });

        $compras = AtivoCompra::orderByDesc('created_at')->get()->map(function ($compra) {
            return [
                'id'             => $compra->id,
                'ativo_id'       => $compra->ativo_id,
                'quantidade'     => $compra->quantidade,
                'valor_unitario' => $compra->valor_unitario,
                'nota_fiscal_id' => $compra->nota_fiscal_id,
                'created_at'     => $compra->created_at->format('d-m-Y H:i:s'),
                'updated_at'     => $compra->updated_at->format('d-m-Y H:i:s'),
            ];
        });

        $notasFiscais = NotaFiscal::all()->map(function ($notaFiscal) {
            return [
                'id'                 => $notaFiscal->id,
                'numero_nota_fiscal' => $notaFiscal->numero_nota_fiscal,
                'fornecedor'         => $notaFiscal->fornecedor_id,
                'comprado_at'        => $notaFiscal->comprado_at->format('d-m-Y'),
            ];
        });

        return Inertia::render('Central/Compra/Index', [
            'compras'      => $compras,
            'ativos'       => $ativos,
            'notasFiscais' => $notasFiscais,
            'permissoes' => [
                'store'   => PermissoesCompras::STORE,
                'update'  => PermissoesCompras::UPDATE,
                'destroy' => PermissoesCompras::DESTROY,
            ]
        ]); 
    }

    public function store(AtivoCompraStoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            AtivoCompra::create($request->validated());
        });

        return Redirect::route('central.compras.index')->with('success', 'Compra criada com sucesso!');
    }

    public function update(AtivoCompraUpdateRequest $request, AtivoCompra $compra)
    {
        DB::transaction(function () use ($request, $compra) {
            $compra->update($request->validated());
        });

        return Redirect::route('central.compras.index')->with('success', 'Compra atualizada com sucesso!');
    }

    public function destroy(AtivoCompra $compra)
    {
        abort_unless(authenticatedUserHasPermission(PermissoesCompras::DESTROY), 403);

        $transferencias = $compra->ativo()->first()->transferencias()->whereNull('unidade_origem')->get();

        /**
         * Se a data da compra for superior a data da ultima transferência, ou seja,
         * foi feita uma compra após os ativos já estarem destribuidos
         * Podemos então excluir a compra, caso contrario não podemos
         */
        foreach($transferencias as $transferencia) {
            if (
                $transferencia->created_at->gte($compra->created_at) ||
                $transferencia->updated_at->gte($compra->created_at) ||
                $transferencia->created_at->gte($compra->updated_at) ||
                $transferencia->updated_at->gte($compra->updated_at)
            ) {
                return Redirect::route('central.compras.index')->with('error', 'Não é possível excluir a compra de um ativo com transferências!');
            }
        }

        $compra->delete();

        return Redirect::route('central.compras.index')->with('success', 'Compra excluida com sucesso!');
    }
}
