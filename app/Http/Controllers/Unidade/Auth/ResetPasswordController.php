<?php

namespace App\Http\Controllers\Unidade\Auth;

use Inertia\Inertia;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    protected function redirectTo()
    {
        return Str::replace('*', tenant('id'), RouteServiceProvider::HOME);
    }

    public function showResetForm(Request $request)
    {
        $token = $request->route()->parameter('token');

        return Inertia::render('Unidade/Auth/Passwords/Reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
}
