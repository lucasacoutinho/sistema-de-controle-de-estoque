<?php

namespace App\Http\Controllers\Unidade\Ativo;

use Inertia\Inertia;
use App\Models\Categoria;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Domain\Permissoes\PermissoesAtivos;

class AtivoController extends Controller
{
    public function index()
    {
        abort_unless(authenticatedUserHasPermission(PermissoesAtivos::INDEX), 403);

        $categorias = Categoria::all()->map(function ($categoria) {
            return ['id' => $categoria->id, 'titulo' => $categoria->titulo];
        });

        $ativos = tenant()->ativos()->with(['detalhe', 'categorias'])->orderByDesc('created_at')->get()->map(function ($ativo) {
            return [
                'id'                 => $ativo->id,
                'titulo'             => $ativo->titulo,
                'descricao'          => Str::limit($ativo->descricao, 20, '...'),
                'n_patrimonio'       => $ativo->detalhe->n_patrimonio,
                'n_serie'            => $ativo->detalhe->n_serie,
                'n_etiqueta_servico' => $ativo->detalhe->n_etiqueta_servico,
                'categorias'         => $ativo->categorias->pluck('id'),
                'created_at'         => $ativo->created_at->format('d-m-Y H:i:s'),
                'updated_at'         => $ativo->updated_at->format('d-m-Y H:i:s'),
            ];
        });

        return Inertia::render('Unidade/Ativo/Index', [
            'ativos'     => $ativos,
            'categorias' => $categorias,
            'permissoes' => [
                'store'   => PermissoesAtivos::STORE,
                'update'  => PermissoesAtivos::UPDATE,
                'destroy' => PermissoesAtivos::DESTROY,
            ]
        ]);
    }
}
