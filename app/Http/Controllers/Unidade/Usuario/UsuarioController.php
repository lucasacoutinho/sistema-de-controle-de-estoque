<?php

namespace App\Http\Controllers\Unidade\Usuario;

use App\Models\User;
use Inertia\Inertia;
use Domain\Funcoes\Funcoes;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Domain\Permissoes\PermissoesUsuarios;
use App\Http\Requests\Unidade\Usuario\UsuarioStoreRequest;
use App\Http\Requests\Unidade\Usuario\UsuarioUpdateRequest;

class UsuarioController extends Controller
{
    public function index()
    {
        abort_unless(authenticatedUserHasPermission(PermissoesUsuarios::INDEX), 403);

        $usuarios = tenant()->usuarios()->orderByDesc('created_at')->get()->map(function ($usuario) {
            return [
                'id'         => $usuario->id,
                'nome'       => $usuario->name,
                'email'      => $usuario->email,
                'unidade'    => tenant('id'),
                'created_at' => $usuario->created_at->format('d-m-Y H:i:s'),
                'updated_at' => $usuario->updated_at->format('d-m-Y H:i:s'),
            ];
        });

        return Inertia::render('Unidade/Usuario/Index', [
            'usuarios'   => $usuarios,
            'permissoes' => [
                'store'   => PermissoesUsuarios::STORE,
                'update'  => PermissoesUsuarios::UPDATE,
                'destroy' => PermissoesUsuarios::DESTROY,
            ]
        ]);
    }

    public function store(UsuarioStoreRequest $request)
    {
        DB::transaction(function () use ($request) {
            $usuario = User::create($request->validated());

            return $usuario;
        });

        return Redirect::route('unidade.usuarios.index')->with('success', 'Usuário criado com sucesso!');
    }

    public function update(UsuarioUpdateRequest $request, User $usuario)
    {
        DB::transaction(function () use ($request, $usuario) {
            $usuario->update($request->validated());

            return $usuario;
        });

        return Redirect::route('unidade.usuarios.index')->with('success', 'Usuário atualizado com sucesso!');
    }

    public function destroy(User $usuario)
    {
        abort_unless(authenticatedUserHasPermission(PermissoesUsuarios::DESTROY), 403);

        if($usuario->hasRole(Funcoes::ADMINISTRADOR)) {
            return Redirect::route('central.usuarios.index')->with('error', 'Não é possível excluir usuário administrador!');
        }

        if($usuario === getAuthenticatedUser()) {
            return Redirect::route('central.usuarios.index')->with('error', 'Não é possível excluir o proprio usuário!');
        }

        $usuario->delete();

        return Redirect::route('unidade.usuarios.index')->with('success', 'Usuário excluido com sucesso!');
    }
}
