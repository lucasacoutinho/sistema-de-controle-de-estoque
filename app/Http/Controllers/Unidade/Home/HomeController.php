<?php

namespace App\Http\Controllers\Unidade\Home;

use Inertia\Inertia;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        return Inertia::render('Unidade/Home/Index');
    }
}
