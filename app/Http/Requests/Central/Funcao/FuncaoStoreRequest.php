<?php

namespace App\Http\Requests\Central\Funcao;

use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Domain\Permissoes\PermissoesFuncoes;
use Illuminate\Foundation\Http\FormRequest;

class FuncaoStoreRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesFuncoes::STORE);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'name'        => Str::ucfirst(Str::lower($this->nome)),
            'permissions' => $this->permissoes,
        ]);
    }

    public function rules()
    {
        return [
            'name'          => ['required', 'string', 'max:255', Rule::unique('roles', 'name')],
            'permissions'   => ['nullable', 'array'],
            'permissions.*' => ['required', 'integer', Rule::exists('permissions', 'id')],
        ];
    }
}
