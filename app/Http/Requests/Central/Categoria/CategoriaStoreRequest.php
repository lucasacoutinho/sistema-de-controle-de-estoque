<?php

namespace App\Http\Requests\Central\Categoria;

use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Domain\Permissoes\PermissoesCategorias;
use Illuminate\Foundation\Http\FormRequest;

class CategoriaStoreRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesCategorias::STORE);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'titulo' => trim(Str::lower($this->titulo)),
        ]);
    }

    public function rules()
    {
        return [
            'titulo' => ['required', 'string', 'max:255', Rule::unique('categorias', 'titulo')]
        ];
    }
}
