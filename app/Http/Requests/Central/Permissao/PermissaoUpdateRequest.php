<?php

namespace App\Http\Requests\Central\Permissao;

use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Domain\Permissoes\PermissoesPermissoes;
use Illuminate\Foundation\Http\FormRequest;

class PermissaoUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesPermissoes::UPDATE);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'name' => Str::lower($this->nome),
        ]);
    }

    public function rules()
    {
        return [
            'name' => ['filled', 'string', 'max:255', Rule::unique('permissions', 'name')->ignore($this->permissao)],
        ];
    }
}
