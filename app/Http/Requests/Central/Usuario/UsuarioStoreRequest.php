<?php

namespace App\Http\Requests\Central\Usuario;

use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Domain\Permissoes\PermissoesUsuarios;
use Illuminate\Foundation\Http\FormRequest;

class UsuarioStoreRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesUsuarios::STORE);
    }

    public function rules()
    {
        return [
            'name'      => ['required', 'string', 'max:255'],
            'email'     => ['required', 'string', 'email', 'max:255', Rule::unique('users', 'email')->where('tenant_id', $this->input('unidade'))],
            'unidade'   => ['nullable', 'string', Rule::exists('tenants', 'id')],
            'funcao'    => ['nullable', 'integer', Rule::exists('roles', 'id')]
        ];
    }

    public function validated(): array
    {
        if ($this->has('unidade')) {
            return array_merge(parent::validated(), [
                'password'  => Hash::make(Str::random(10)),
                'tenant_id' => $this->input('unidade')
            ]);
        }

        return array_merge(parent::validated(), ['password' => Hash::make(Str::random(10))]);
    }
}
