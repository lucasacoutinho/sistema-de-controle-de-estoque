<?php

namespace App\Http\Requests\Central\Compra;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Domain\Permissoes\PermissoesNotasFiscais;

class NotaFiscalStoreRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesNotasFiscais::STORE);
    }

    public function rules()
    {
        return [
            'numero_nota_fiscal' => ['required', 'string', 'size:44'],
            'fornecedor_id'      => ['required', 'integer', Rule::exists('fornecedores', 'id')],
            'comprado_at'        => ['required', 'date', 'date_format:Y-m-d'],
        ];
    }
}
