<?php

namespace App\Http\Requests\Central\Compra;

use Illuminate\Validation\Rule;
use Domain\Permissoes\PermissoesCompras;
use Illuminate\Foundation\Http\FormRequest;

class AtivoCompraStoreRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesCompras::STORE);
    }

    public function rules()
    {
        return [
            'ativo_id'       => ['required', 'integer', Rule::exists('ativos', 'id')],
            'nota_fiscal_id' => ['required', 'integer', Rule::exists('compra_nota_fiscal', 'id')],
            'quantidade'     => ['required', 'integer', 'min:1'],
            'valor_unitario' => ['required', 'numeric', 'min:0'],
        ];
    }
}
