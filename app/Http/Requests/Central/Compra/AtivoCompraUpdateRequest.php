<?php

namespace App\Http\Requests\Central\Compra;

use Illuminate\Validation\Rule;
use Domain\Permissoes\PermissoesCompras;
use Illuminate\Foundation\Http\FormRequest;

class AtivoCompraUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesCompras::UPDATE);
    }

    public function rules()
    {
        return [
            'ativo_id'       => ['filled', 'integer', Rule::exists('ativos', 'id')],
            'nota_fiscal_id' => ['filled', 'integer', Rule::exists('compra_nota_fiscal', 'id')],
            'quantidade'     => ['filled', 'integer', 'min:1'],
            'valor_unitario' => ['filled', 'numeric', 'min:0'],
        ];
    }
}
