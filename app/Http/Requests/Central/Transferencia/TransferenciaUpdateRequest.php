<?php

namespace App\Http\Requests\Central\Transferencia;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use App\Rules\Central\Transferencia\Estoque;
use Domain\Permissoes\PermissoesTransferencias;

class TransferenciaUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesTransferencias::UPDATE);
    }

    public function rules()
    {
        return [
            'ativo_id'   => ['filled', 'integer', Rule::exists('ativos', 'id')],
            'unidade'    => ['filled', 'string', Rule::exists('tenants', 'id')],
            'quantidade' => ['filled', 'integer', 'min:1', new Estoque($this->ativo_id, $this->transferencia)],
        ];
    }


    public function validated(): array
    {
        if ($this->has('unidade')) {
            return array_merge(parent::validated(), ['unidade_destino' => $this->input('unidade')]);
        }

        return parent::validated();
    }
}
