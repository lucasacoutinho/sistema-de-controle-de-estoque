<?php

namespace App\Http\Requests\Central\Fornecedor;

use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Domain\Permissoes\PermissoesFornecedores;

class FornecedorStoreRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesFornecedores::STORE);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'titulo' => trim(Str::lower($this->titulo)),
        ]);
    }

    public function rules()
    {
        return [
            'titulo' => ['required', 'string', 'max:255', Rule::unique('fornecedores', 'titulo')]
        ];
    }
}
