<?php

namespace App\Http\Requests\Unidade\Usuario;

use Illuminate\Validation\Rule;
use Domain\Permissoes\PermissoesUsuarios;
use Illuminate\Foundation\Http\FormRequest;

class UsuarioUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesUsuarios::UPDATE);
    }

    public function rules()
    {
        return [
            'name'  => ['filled', 'string', 'max:255'],
            'email' => ['filled', 'string', 'email', 'max:255', Rule::unique('users', 'email')->where('tenant_id', tenant('id'))->ignore($this->usuario)],
        ];
    }
}
