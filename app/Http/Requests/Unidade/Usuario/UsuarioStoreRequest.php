<?php

namespace App\Http\Requests\Unidade\Usuario;

use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Domain\Permissoes\PermissoesUsuarios;
use Illuminate\Foundation\Http\FormRequest;

class UsuarioStoreRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesUsuarios::STORE);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'tenant_id' => tenant('id'),
        ]);
    }

    public function rules()
    {
        return [
            'name'      => ['required', 'string', 'max:255'],
            'email'     => ['required', 'string', 'email', 'max:255', Rule::unique('users', 'email')->where('tenant_id', tenant('id'))],
            'tenant_id' => ['required', 'string', Rule::exists('tenants', 'id')],
        ];
    }

    public function validated(): array
    {
        return array_merge(parent::validated(), ['password' => Hash::make(Str::random(10))]);
    }
}
