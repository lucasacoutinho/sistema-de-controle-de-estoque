<?php

namespace App\Http\Requests\Unidade\Transferencia;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use App\Rules\Unidade\Transferencia\Estoque;
use Domain\Permissoes\PermissoesTransferencias;

class TransferenciaUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return authenticatedUserHasPermission(PermissoesTransferencias::UPDATE);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'unidade_origem' => tenant('id'),
        ]);
    }

    public function rules()
    {
        return [
            'ativo_id'       => ['filled', 'integer', Rule::exists('tenant_ativos', 'ativo_id')->where('tenant_id', tenant('id'))],
            'unidade_origem' => ['filled', 'string', Rule::exists('tenants', 'id')],
            'unidade'        => ['required', 'string', Rule::exists('tenants', 'id')],
            'quantidade'     => ['filled', 'integer', 'min:1', new Estoque($this->ativo_id, tenant('id'), $this->transferencia->id)],
        ];
    }


    public function validated(): array
    {
        if ($this->has('unidade')) {
            return array_merge(parent::validated(), ['unidade_destino' => $this->input('unidade')]);
        }

        return parent::validated();
    }
}
