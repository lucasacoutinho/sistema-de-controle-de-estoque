<?php

namespace App\Rules\Central\Transferencia;

use Domain\Status\Status;
use App\Models\AtivoCompra as Compra;
use Illuminate\Contracts\Validation\Rule;
use App\Models\AtivoUnidade as TransferenciaCompleta;
use App\Models\Transferencia as TransferenciaPendente;

class Estoque implements Rule
{
    private $ativo;
    private $transferencia;

    public function __construct($ativo, $transferencia = null)
    {
        $this->ativo         = $ativo;
        $this->transferencia = $transferencia;
    }

    public function passes($attribute, $value)
    {
        return $value <= $this->calcularEstoqueAtual();
    }

    private function calcularEstoqueTotal()
    {
        return Compra::query()->where('ativo_id', $this->ativo)->sum('quantidade');
    }

    private function calcularEstoqueUtilizado()
    {
        $estoqueTransferido     = TransferenciaCompleta::query()->where('ativo_id', $this->ativo)
                                                                ->sum('quantidade');

        $estoqueEmTransferencia = TransferenciaPendente::query()
                                                            ->when($this->transferencia, function ($query) {
                                                                return $query->where('id', '!=', $this->transferencia->id);
                                                            })
                                                            ->whereNull('unidade_origem')
                                                            ->where('ativo_id', $this->ativo)
                                                            ->where('status', Status::PENDENTE)
                                                            ->sum('quantidade');

        return $estoqueTransferido + $estoqueEmTransferencia;
    }

    private function calcularEstoqueAtual()
    {
        return $this->calcularEstoqueTotal() - $this->calcularEstoqueUtilizado();
    }

    public function message()
    {
        return 'The :attribute value must be less or equal to the current stock.';
    }
}
