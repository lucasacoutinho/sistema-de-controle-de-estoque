<?php

namespace App\Console\Commands\Permissoes\Administrador;

use Throwable;
use Domain\Funcoes\Funcoes;
use Illuminate\Console\Command;
use Domain\Comandos\Permissoes\Administrador\Administrador;

class Remover extends Command
{
    protected $signature = 'permissoes-administrador:remover {usuario_id}';

    protected $description = 'Remover permissões de administrador ao usuário informado';

    public function handle()
    {
        $usuario_id = $this->argument('usuario_id');

        try {
            (new Administrador($usuario_id))->remover();
            $this->info('Permissão de ' . Funcoes::ADMINISTRADOR . ' removida com sucesso do usuário informado.');

            return Command::SUCCESS;
        } catch (Throwable $e) {
            $this->error('Não foi possível removida a permissão de ' . Funcoes::ADMINISTRADOR. ' do usuário informado.');

            return Command::FAILURE;
        }
    }
}
