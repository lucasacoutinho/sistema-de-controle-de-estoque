<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Ativo extends Model
{
    use HasFactory;

    protected $table = 'ativos';

    protected $fillable = [
        'titulo',
        'descricao'
    ];

    public function categorias(): BelongsToMany
    {
        return $this->belongsToMany(Categoria::class, 'ativo_categorias', 'ativo_id', 'categoria_id');
    }

    public function detalhe(): HasOne
    {
        return $this->hasOne(AtivoDetalhe::class, 'ativo_id');
    }

    public function compras(): HasMany
    {
        return $this->hasMany(AtivoCompra::class, 'ativo_id');
    }

    public function transferencias(): HasMany
    {
        return $this->hasMany(Transferencia::class, 'ativo_id');
    }

    public function unidades(): BelongsToMany
    {
        return $this->belongsToMany(Tenant::class, 'tenant_ativos', 'ativo_id', 'tenant_id');
    }
}
