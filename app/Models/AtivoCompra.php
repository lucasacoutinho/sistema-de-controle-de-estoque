<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AtivoCompra extends Pivot
{
    protected $table = 'compra_ativos';

    protected $fillable = [
        'ativo_id',
        'nota_fiscal_id',
        'quantidade',
        'valor_unitario',
    ];

    protected $casts = [
        'valor_unitario' => 'float',
    ];

    public function ativo(): BelongsTo
    {
        return $this->belongsTo(Ativo::class, 'ativo_id');
    }

    public function notaFiscal(): BelongsTo
    {
        return $this->belongsTo(NotaFiscal::class, 'nota_fiscal_id');
    }
}
