<?php

namespace App\Models;

use Domain\Status\Status;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Transferencia extends Model
{
    use HasFactory;

    protected $table = 'transferencias';

    protected $fillable = [
        'ativo_id',
        'unidade_origem',
        'unidade_destino',
        'quantidade',
        'status'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->status = Status::PENDENTE;
        });

        static::updating(function($model)  {
            if($model->status === Status::CONFIRMADA){
                # Adiciona o ativo na unidade de destino
                $ativoJaPresenteNaUnidade = AtivoUnidade::where('ativo_id', $model->ativo_id)->where('tenant_id', $model->unidade_destino)->first();

                if(is_null($ativoJaPresenteNaUnidade)) {
                    AtivoUnidade::create([
                        'ativo_id'   => $model->ativo_id,
                        'tenant_id'  => $model->unidade_destino,
                        'quantidade' => $model->quantidade,
                    ]);
                } else {
                    $ativoJaPresenteNaUnidade->update([
                        'quantidade' => $ativoJaPresenteNaUnidade->quantidade + $model->quantidade
                    ]);
                }

                #Remove estoque da unidade de origem
                if($model->unidade_origem) {
                    $ativoOrigem = AtivoUnidade::where('ativo_id', $model->ativo_id)->where('tenant_id', $model->unidade_origem)->first();
                    $ativoOrigem->update([
                        'quantidade' => $ativoOrigem->quantidade - $model->quantidade
                    ]);
                }
            }
        });
    }

}
