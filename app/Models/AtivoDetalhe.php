<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AtivoDetalhe extends Model
{
    use HasFactory;

    protected $table = 'ativo_detalhes';

    protected $fillable = [
        'n_patrimonio',
        'n_serie',
        'n_etiqueta_servico'
    ];
}
